#!/usr/bin/env groovy
// MYAPP - this is an example of a php-tarball style deploy, done with 'gatekeeper' under the bonnet

pipeline {

    agent {
        label 'lkg' // for MYAPP, change to 'nbl' depending on AWS account
    }

    options {
         timestamps ()
    }

    environment {
        // GIT_URL and GIT_BRANCH are provided by Jenkins
        ENV = 'master'
        NPM_RUN = "${env.ENV}"
        component = 'jenkins-test'/*
        aws_account = 'lkg'
        jenkins_git_credentialsId = 'b9a25622-bf4f-4230-8a9a-5830e9021885'
        slack_channel = "#deployment"
        wpengine_env_label = "signotesting"*/
        // for secret type user/pass, this will create three vars:
        // - wpengine_creds = ''
        // - wpengine_creds_USR = ''
        // - wpengine_creds_PSW = ''

        /* i'm getting an error on this line*/
       /* wpengine_creds = credentials("wpengine-${env.component}-${env.ENV}-${env.wpengine_env_label}") // TODO: un-paul this!
        sftp_host = "signotesting.sftp.wpengine.com"
        sftp_port = "2222"
        source_dir = "sftp-src/"*/

        /* these vars are for sshPut, for sftp'ing, but there isn't any
           documentation for using sshPut in a declarative jenkinsfile
        remote = [:]
        remote.name = "${env.wpengine_env_label}"
        remote.host = "${env.sftp_host}"
        remote.port = "${env.sftp_port}"
        remote.user = "${env.wpengine_creds_USR}"
        remote.password = "${env.wpengine_creds_PSW}"
        */
    }

    parameters {
        booleanParam(name:"EXCLUDE_CORE_WP_FILES", defaultValue: true, description:"Excludes all files from .gitignore from the final package, - the core WP files are in .gitignore (as recommended by WPEngine)\n WARNING: disabling this checkbox will overwrite any core files on WPEngine, potentially rolling back WordPress")
        choice(name: 'ACTION', choices: ['deploy', 'rollback'], description: 'Do you want to deploy... or roll back?')
        string(name: "ROLLBACK_PACKAGE", defaultValue: '', description: "Package FULL FILENAME for ROLLBACK ONLY. Leave empty for deploys.\nThis package MUST already exist on S3 (${env.s3_package_dir}... or you can get the filename out of previous buildlogs, or the index file on s3)")
        booleanParam(name:"CLEAN_BEFORE_BUILD", defaultValue: false, description:"Runs `git clean -fdx` which destroy all files not part of the git commit.\n NOTE: This will remove files such as composer.lock, node_modules/, vendor/")
        string(name: 'BRANCH', defaultValue: "origin/master", description: "Git branch to use (no need to specify 'origin/')") // parameter default needs 'origin/' prefix, or you will get a 'change branch' step every run
    }

    stages{
        stage('change-branch') {
            when {
                   not { equals expected: "${env.GIT_BRANCH}", actual: "${params.BRANCH}" }
            }
            steps {
                git branch: "${params.BRANCH}",
                    credentialsId: jenkins_git_credentialsId,
                    url: "${env.GIT_URL}"
            }
        }
        stage('clean'){
            when {
                environment name: 'CLEAN_BEFORE_BUILD', value: 'true'
            }
            steps {
                echo "=====> Cleaning workspace"

                //destroy all files not part of the git commit. ref: https://git-scm.com/docs/git-clean
                sh "git clean -fdx"
            }

        }
        stage('build'){
            when {
                environment name: 'ACTION', value: 'deploy'
            }
            steps {
                sh """
                    if [ -n "$ROLLBACK_PACKAGE" ]; then
                        echo "=====> Deploy requested but a ROLLBACK_PACKAGE var has been supplied. A deploy is probably not what you want. Aborting"
                        exit 1
                    fi
                """

                // remove this stage if we start keeping secrets in env
                echo "=====> Build environment"
                sh "printenv | sort"

                echo "=====> Building ${env.ENV} package"

                sh 'make package'

                echo "=====> uploading package"

                sh 'make upload'

                sh 'make clean'
            }
        }
        stage('rollback'){
            when {
                environment name: 'ACTION', value: 'rollback'
            }
            steps {
                sh """
                    if [ -z "$ROLLBACK_PACKAGE" ]; then
                        echo "=====> No ROLLBACK_PACKAGE var supplied. Aborting"
                        exit 1
                    fi
                """

                sh """
                    if ! aws s3 ls "${env.s3_package_dir}/$ROLLBACK_PACKAGE" >/dev/null; then
                        echo "$ROLLBACK_PACKAGE does not exist on S3. Try again."
                        exit 1
                    fi
                """

                sh """
                    echo "=====> prepping sftp-src directory with $ROLLBACK_PACKAGE"
                    TARFILE=$ROLLBACK_PACKAGE make prep-deploy
                """
            }
        }
        stage('prep-deploy'){
            when {
                environment name: 'ACTION', value: 'deploy'
            }
            steps {
                echo "=====> prepping sftp-src directory..."
                sh 'make prep-deploy-dir'
            }
        }

        stage('update wpengine') {
            steps {
                echo "=====> deploying ${env.source_dir} to ${env.wpengine_env_label}..."

                /* can't figure out how to make sshPut work in a declarative jenkinsfile
                sshPut remote: remote, from: "${env.source_dir}", into '.'
                */

                /*
                    This has problems - the wildcard acts as a shell glob, and so scp might
                    try to copy found files before their parent directories are created, which
                    causes an error. Basically this means expect errors if you have a new directory,
                    and keep on re-running the deploy until those 'No such file or directory' errors
                    go away. Additionally, you lose out on copying dotfiles.

                    If you remove the wildcard, this problem goes away, but now it will copy the full
                    listed path. If you cd into the directory first and use '.' to recurse from
                    'current directory', you will get an error from trying to copy `.` itself, as
                    you can't set the mode on the server

                    Additionally, we have to turn off strict hostkey checking because on a FIRST
                    run, the key won't be present. Scp correctly errors on this, but sshpass blocks
                    the error - you just get a silent failure. We don't have a mechanism for devs
                    to easily add the ssh key to authorized_keys, so we do it this way. At least
                    the hostnames will be unique for each deploy.

                    At time of writing, Feb '19, I haven't found a clean way to do this.

                    TODO: recursive sftp that copies cleanly, including dotfiles
                    TODO: fix the hostkey checking problem
                */
                // echo "WARNING: if you have new directories, you may get errors like 'No Such File Or Directory'. Rerun the plan (it could happen once per level of directory). See the comment in the Jenkinsfile for more information"
                // sh "sshpass -p ${env.wpengine_creds_PSW} scp -r -o StrictHostKeyChecking=no -P ${env.sftp_port} ${env.source_dir}/* ${env.wpengine_creds_USR}@${env.sftp_host}:"

                //sh "sshpass -p ${env.wpengine_creds_PSW} scp -r -P ${env.sftp_port} ${env.source_dir} ${env.wpengine_creds_USR}@${env.sftp_host}"
                // sh "wpengine-copy -u ${env.wpengine_creds_USR} -p ${env.wpengine_creds_PSW} ${env.source_dir} ${env.sftp_host} ${env.sftp_port}"
            }
        }
    }
}
