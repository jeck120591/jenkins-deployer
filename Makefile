.PHONY: package upload clean full-clean package-name prep-builddir

# This makefile packages up this repo as a Wordpress tarfile, either with or
# without the 'wordpress core files'. It also preps a 'deploy' directory, that
# Jenkins can use to sftp files from (using credentials stored in Jenkins)

# WARNING: the packaging in this Makefile will default to exclude everything in the
# .gitignore file, including all the core WP files (as recommended by WPEngine)

# You can deploy a known package by passing the name as the env var TARFILE


###
### VARIABLES
###

COMPONENT := jenkins-test
BUILD_DIR := build
SFTP_DIR := sftp-src
# AWSCLI_PROFILE ?= lkg
ENV ?= master

## using Jenkins build vars to track versions, but these are placeholders if
## not running under jenkins
JOB_NAME ?= $(ENV)
BUILD_NUMBER ?= $(shell git rev-parse --short HEAD)
## alternatively, if we wanted to track versions using git annotated tags
#BUILD_NUMBER := $(shell git describe --dirty)


VERSION := $(JOB_NAME)_$(BUILD_NUMBER)
empty :=
space := $(empty) $(empty)
TARFILE ?= $(subst $(space),-,$(COMPONENT)_$(VERSION).tar.gz)
EXCLUDE_WP_CORE_FILES ?= true
ifeq ($(EXCLUDE_WP_CORE_FILES), true)
TARFILE := wpe_$(TARFILE)
endif

S3_TARFILE := E:/test-packages/wordpress/$(COMPONENT)/$(ENV)/$(TARFILE)


###
### TARGETS
###

all: package


##
## PACKAGE STEPS
##

package: $(TARFILE)

$(TARFILE): prep-builddir
		@echo "===> Packaging application as '$(TARFILE)'..."
		tar zcf $(TARFILE) -C $(BUILD_DIR) .

# Windows does not accept rsync
ifeq ($(EXCLUDE_WP_CORE_FILES), true)
prep-builddir:
	@echo "===> copying files to build dir ready for packaging..."
	@echo "===> CORE WORDPRESS FILES NOT INCLUDED (recommended by WPEngine)"
	rm -rf $(BUILD_DIR)
	mkdir -p $(BUILD_DIR)
	rsync -a \
		--include '/.htaccess' \
		--exclude '/.git*' \
		--exclude '*.tar.gz' \
		--exclude $(BUILD_DIR) \
		--exclude $(SFTP_DIR) \
		--exclude deploy \
		--exclude Makefile \
		--exclude-from .gitignore \
		. \
		$(BUILD_DIR)
else
prep-builddir:
	@echo "===> copying files to build dir ready for packaging..."
	rm -rf $(BUILD_DIR)
	mkdir -p $(BUILD_DIR)
	rsync -a \
	    --include '/.htaccess' \
		--exclude '/.git*' \
		--exclude '*.tar.gz' \
		--exclude $(BUILD_DIR) \
		--exclude $(SFTP_DIR) \
		--exclude deploy \
		--exclude Makefile \
		. \
		$(BUILD_DIR)
endif


##
## UPLOAD PACKAGE TO REPO
##
# upload and register are separate steps so you can have buildplans
# that upload packages, but don't automatically update the version to install

upload:
	#aws s3 cp --quiet $(TARFILE) $(S3_TARFILE)
	cp --quiet $(TARFILE) $(S3_TARFILE)

download:
	#aws s3 cp --quiet $(S3_TARFILE) .
	cp --quiet $(S3_TARFILE) .

# This is done in the makefile instead of the Jenkinsfile as Jenkins won't
# necessarily know which $(TARFILE) to grab
prep-deploy-dir: download
	rm -rf $(SFTP_DIR)
	mkdir -p $(SFTP_DIR)
	tar -zxf $(TARFILE) -C $(SFTP_DIR)
	# TODO: sftp stuff here ... or in Jenkinsfile?



##
## UTILS
##

package-name:
	@echo $(TARFILE)


clean:
	rm -rf $(BUILD_DIR) $(SFTP_DIR)
	rm -f *.tar.gz *.gz

full-clean:
	git clean -fdx
